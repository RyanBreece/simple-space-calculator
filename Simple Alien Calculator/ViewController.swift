//
//  ViewController.swift
//  Simple Alien Calculator
//
//  Created by Ryan Breece on 3/28/16.
//  Copyright © 2016 Ryan Breece. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    enum Operation: String {
        case Divide = "/"
        case Multiply = "*"
        case Add = "+"
        case Subtract = "-"
        case Reset = "Reset"
        case Empty = "empty"
    }
    
    @IBOutlet weak var outputDisplay: UILabel!
    
    var btnSound: AVAudioPlayer!
    var btnResetSound: AVAudioPlayer!
    
    var runningNumber = ""
    var leftNumber = ""
    var rightNumber = ""
    var currentOperation: Operation = Operation.Empty
    var result = ""
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let resetSoundPath = NSBundle.mainBundle().pathForResource("btn-reset", ofType: "mp3")
        
        let btnSoundPath = NSBundle.mainBundle().pathForResource("switch7", ofType: "wav")
        
        let resetSoundUrl = NSURL(fileURLWithPath: resetSoundPath!)
        
        let btnSoundUrl = NSURL(fileURLWithPath: btnSoundPath!)
        
        do {
            try btnSound = AVAudioPlayer(contentsOfURL: btnSoundUrl)
            btnSound.prepareToPlay()
        } catch let btnSoundError as NSError {
            print(btnSoundError.debugDescription)
        }
        
        do {
            try btnResetSound = AVAudioPlayer(contentsOfURL: resetSoundUrl)
            btnResetSound.prepareToPlay()
        } catch let resetSoundErr as NSError{
            print(resetSoundErr.debugDescription)
        }
        
    }

    @IBAction func buttonPressed(btn: UIButton!) {
        
        playBtnSound()
        
        runningNumber += "\(btn.tag)"
        outputDisplay.text = runningNumber
    }

    @IBAction func onAdditionPress(sender: AnyObject) {
        processOperation(Operation.Add)
        
    }
    
    @IBAction func onSubtractPress(sender: AnyObject) {
        processOperation(Operation.Subtract)
        
    }
    
    @IBAction func onMultiplyPress(sender: AnyObject) {
        processOperation(Operation.Multiply)
        
    }
    
    @IBAction func onDividePress(sender: AnyObject) {
        processOperation(Operation.Divide)
        
    }
    
    @IBAction func onEqualsPress(sender: AnyObject) {
        processOperation(currentOperation)
        
    }
    
    @IBAction func onResetPress(sender: AnyObject) {
        playResetSound()
        resetAll()
    }
    
    func processOperation (op: Operation) {
        playBtnSound()
        
        if currentOperation != Operation.Empty {
            
            if runningNumber != "" {
              
                rightNumber = runningNumber
                runningNumber = ""
                
                if currentOperation == Operation.Add {
                    result = "\(Int(leftNumber)! + Int(rightNumber)!)"
                    
                }else if currentOperation == Operation.Subtract {
                    result = "\(Int(leftNumber)! - Int(rightNumber)!)"
                    
                }else if currentOperation == Operation.Multiply {
                    result = "\(Int(leftNumber)! * Int(rightNumber)!)"
                    
                }else if currentOperation == Operation.Divide {
                    result = "\(Int(leftNumber)! / Int(rightNumber)!)"
                }
                
                leftNumber = result
                outputDisplay.text = result
            }
            
            currentOperation = op
            
        } else {
            currentOperation = op
            leftNumber = runningNumber
            runningNumber = ""
            
        }
    }
    
    func playBtnSound() {
        
        if btnSound.playing {
            btnSound.stop()
        }
        
        btnSound.play()
    }
    
    func playResetSound() {
        
        if btnResetSound.playing {
            btnResetSound.stop()
        }
        
        btnResetSound.play()
    }
    
    func resetAll () {
        
        runningNumber = ""
        leftNumber = ""
        rightNumber = ""
        currentOperation = Operation.Empty
        result = ""
        outputDisplay.text = ""
        
    }
}



















